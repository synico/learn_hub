### Atom插件
Atom的插件实际上是在npm上，npm的官方源在国内访问是非常缓慢的。但是国内有许多镜像源可以使用，如淘宝源。可使用下面命令设置apm的更新源。

`apm config set registry http://registry.npm.taobao.org/`

`apm config set registry http://r.cnpmjs.org/`
